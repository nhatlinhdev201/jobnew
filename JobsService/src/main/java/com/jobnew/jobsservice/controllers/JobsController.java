package com.jobnew.jobsservice.controllers;

import com.jobnew.jobsservice.models.Job;
import com.jobnew.jobsservice.models.User;
import com.jobnew.jobsservice.repositories.JobRepository;
import com.jobnew.jobsservice.services.JobService;
import com.jobnew.jobsservice.services.RedisService;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@RestController
@RequestMapping("/api/job/v1")
public class JobsController {
    @Value("${user-service.url}")
    private String userServiceUrl;

    @Autowired
    private JobService jobService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CircuitBreakerFactory circuitBreakerFactory;

    @Autowired
    private RedisService redisService;


    //RATE LIMITING
    @GetMapping("/users-rate/{id}")
    @RateLimiter(name = "squareLimit",fallbackMethod = "squareErrorResponse")
    public ResponseEntity<?> getUSerRate(@PathVariable("id") String id){
        String url = userServiceUrl + "users/"+id;

        try {
            User user = restTemplate.getForObject(url, User.class, id);
            return ResponseEntity.ok(user);
        }
        catch (HttpServerErrorException.InternalServerError e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Xin lỗi! Không thể kết nối đến service bạn mong muốn");
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Xin lỗi, Không thể kết nối đến service bạn mong muốn.");
        }
    }


    public ResponseEntity<?> squareErrorResponse( Throwable throwable){
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Xin lỗi, bạn đã gọi quá số lần cho phép trong 1 phút. Vui lòng thử lại sau 1 phút.");
    }
    // CIRCUIT BREAKER
    @GetMapping("/users/{id}")
    public ResponseEntity<?> getUSer(@PathVariable("id") String id) {
        try {
            User user = jobService.getUser(id);
            return ResponseEntity.ok(user);
        }
        catch (HttpServerErrorException.InternalServerError e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Xin lỗi! Không thể kết nối đến service bạn mong muốn");
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Xin lỗi, Không thể kết nối đến service bạn mong muốn.");
        }
    }

    @GetMapping("/jobs")
    public ResponseEntity<List<Job>> getAllJob() {
        List<Job> jobs = jobService.getAllJob();
        return new ResponseEntity<>(jobs, HttpStatus.OK);
    }

    @GetMapping("/jobs/{id}")
    public ResponseEntity<Job> getJobById(@PathVariable("id") String id) {
        Optional<Job> job = jobService.getJobById(id);
        System.out.println(job);
        if (job.isPresent()) {
            return new ResponseEntity<>(job.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/jobs")
    public ResponseEntity<Job> createJob(@RequestBody Job job) {
        job.setStatus(true);
        Job created = jobService.createJob(job);
        redisService.save(created);
        return new ResponseEntity<>(created, HttpStatus.CREATED);
    }

    @GetMapping("/jobsRedis/{id}")
    public ResponseEntity<Job> getJobRedisById(@PathVariable("id") String id) {
        Job job = redisService.findById(id);
        List<Job> jobs = redisService.findAll();

        System.out.println(jobs);
        if (job!=null) {
            return new ResponseEntity<>(job, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }





    @PutMapping("/jobs/delete")
    public ResponseEntity<Job> deleteJob(@RequestBody Map<String, Object> requestData) {
        try {
            String id = (String) requestData.get("id");
            Job deleted = jobService.deleteJob(id);
            return new ResponseEntity<>(deleted, HttpStatus.OK);
        } catch (ResponseStatusException ex) {
            return new ResponseEntity<>(ex.getStatusCode());
        }
    }
    @PutMapping("/jobs/update")
    public ResponseEntity<Job> updateJob(@RequestBody Job job) {
        try {
            Job updated = jobService.updateJob(job);
            redisService.update(updated);
            return new ResponseEntity<>(updated, HttpStatus.OK);
        } catch (ResponseStatusException ex) {
            return new ResponseEntity<>(ex.getStatusCode());
        }
    }
}
