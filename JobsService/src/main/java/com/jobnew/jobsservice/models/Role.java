package com.jobnew.jobsservice.models;

public enum Role {
    CANDIDATE("CANDIDATE"),
    HIRING("HIRING");

    private final String value;
    Role(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
