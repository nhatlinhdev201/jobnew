package com.jobnew.jobsservice.services;

import com.jobnew.jobsservice.models.Job;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class RedisService {
    private HashOperations hashOperations;
    private RedisTemplate redisTemplate;

    public RedisService(RedisTemplate redisTemplate) {
        super();
        this.hashOperations = redisTemplate.opsForHash();
        this.redisTemplate = redisTemplate;
    }


    public void save(Job job) {
        hashOperations.put("Job", job.getId(), job);
    }

    public Job findById(String id) {
        return (Job) hashOperations.get("Job", id);
    }

    public List<Job> findAll() {
        return hashOperations.values("Job");
    }

    public void update(Job job) {
        save(job);
    }

    public void delete(String id) {
        hashOperations.delete("Job", id);
    }
}
