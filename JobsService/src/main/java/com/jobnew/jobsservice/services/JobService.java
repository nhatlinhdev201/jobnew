package com.jobnew.jobsservice.services;

import com.jobnew.jobsservice.models.Job;
import com.jobnew.jobsservice.models.User;
import com.jobnew.jobsservice.repositories.JobRepository;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

@Service
public class JobService {
    @Autowired
    private JobRepository jobRepository;
    public Job createJob(Job job) {
        job.setCreateAt(new Date());
        return jobRepository.save(job);
    }
    public Job findJob(String createrId, Date createAt) {
        return jobRepository.findByCreaterIdAndCreateAt(createrId,createAt);
    }

    public List<Job> getAllJob() {
        return jobRepository.findAll();
    }
    public Optional<Job> getJobById(String id) {
        return jobRepository.findById(id);
    }


    private final RestTemplate restTemplate = new RestTemplate();
    private final CircuitBreaker circuitBreaker;

    @Autowired
    public JobService(CircuitBreakerRegistry circuitBreakerRegistry) {
        this.circuitBreaker = circuitBreakerRegistry.circuitBreaker("myService");
    }

    public User getUser(String id) {
        String url = "http://localhost:8082/api/user/v1/users/" + id;

        Supplier<User> decoratedSupplier = CircuitBreaker
                .decorateSupplier(circuitBreaker, () -> restTemplate.getForObject(url, User.class));

        try {
            return decoratedSupplier.get();
        } catch (CallNotPermittedException e) {
            // CircuitBreaker is open
            return fallbackUser(id, e);
        } catch (Exception e) {
            return fallbackUser(id, e);
        }
    }

    public User fallbackUser(String id, Throwable throwable) {
        if (circuitBreaker.getState() == CircuitBreaker.State.CLOSED) {
            return new User(id, "Circuit Breaker is CLOSED ");
        } else if (circuitBreaker.getState() == CircuitBreaker.State.OPEN) {
            return new User(id, "Circuit Breaker is OPEN");
        } else if (circuitBreaker.getState() == CircuitBreaker.State.HALF_OPEN) {
            return new User(id, "Circuit Breaker is HALF OPEN");
        } else {
            return new User(id, "Fallback User");
        }
    }

    public Job deleteJob(String id) {
        if(jobRepository.existsById(id)) {
            Job deletedJob = jobRepository.findById(id).get();
            deletedJob.setStatus(false);
            return jobRepository.save(deletedJob);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"id job is not exits !");
        }
    }
    public Job updateJob(Job job) {
        if(jobRepository.existsById(job.getId())) {
            return jobRepository.save(job);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"id job is not exits !");
        }
    }
}
