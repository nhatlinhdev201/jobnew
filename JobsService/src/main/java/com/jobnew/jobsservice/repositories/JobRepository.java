package com.jobnew.jobsservice.repositories;

import com.jobnew.jobsservice.models.Job;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface JobRepository extends MongoRepository<Job, String> {
    List<Job> findAllByStatus(boolean status);
    Job findByCreaterIdAndCreateAt(String createrId, Date createAt);

}
