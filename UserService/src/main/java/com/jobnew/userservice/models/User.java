package com.jobnew.userservice.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "user")
public class User {
    @Id
    private String id;

    @Field("username")
    private String username;

    @Field("name")
    private String name;

    @Field("email")
    private String email;

    @Field("status")
    private Boolean status;

    @Field("role")
    private Role role;

    @Field("createAt")
    private Date createAt;

    @Field("cv")
    private String cv;
}
