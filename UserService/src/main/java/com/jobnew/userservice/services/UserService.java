package com.jobnew.userservice.services;

import com.jobnew.userservice.models.User;
import com.jobnew.userservice.repositories.UserRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User createUser(User user) {
        return userRepository.save(user);
    }
    public List<User> getAllUser() {
        boolean status = true;
        return userRepository.findAllByStatus(status);
    }
    public Optional<User> getUserById(String id) {
        return userRepository.findById(id);
    }
    public User deleteUser(String id) {
       if(userRepository.existsById(id)) {
           User deletedUser = userRepository.findById(id).get();
           deletedUser.setStatus(false);
           return userRepository.save(deletedUser);
       } else {
           throw new ResponseStatusException(HttpStatus.NOT_FOUND,"id user is not exits !");
       }
    }
    public User updateUser(User user) {
        if(userRepository.existsById(String.valueOf(user.getId()))) {
            return userRepository.save(user);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"id user is not exits !");
        }
    }
}
