package com.jobnew.analyzeservice.controllers;

import com.jobnew.analyzeservice.models.Job;
import com.jobnew.analyzeservice.models.User;

import io.github.resilience4j.retry.Retry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import java.util.Date;
import java.util.function.Supplier;

@RestController
@RequestMapping("/api/analyze/v1")
public class AnalyzeController {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private io.github.resilience4j.retry.Retry retry;
    private static final String ANALYZE_SERVICE = "analyzeService";
    private int attempt=1;
    @GetMapping("/helloworld")
    public ResponseEntity<String> helloWorld() {
        return  new ResponseEntity<>("hello world", HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<?> getUSer(@PathVariable("id") String id) {
        String url = "http://localhost:8082/api/user/v1/users/"+id;
        Supplier<User> supplier = Retry.decorateSupplier(retry, () -> {
            System.out.println("retry called "+ attempt++ +" times at" + new Date());
            return restTemplate.getForObject(url,User.class);
        });
        try {
            User user = supplier.get();
            return ResponseEntity.ok(user);
        }
        catch (HttpServerErrorException.InternalServerError e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Xin lỗi! Không thể kết nối đến service bạn mong muốn.");
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Xin lỗi! Không thể kết nối đến service bạn mong muốn 1.");
        }
    }

    @GetMapping("/jobs/{id}")
    public ResponseEntity<?> getJob(@PathVariable("id") String id) {
        String url = "http://localhost:8083/api/job/v1/jobs/"+id;
        Supplier<Job> supplier = Retry.decorateSupplier(retry, () -> {
            System.out.println("retry called "+ attempt++ +" times at" + new Date());
            return restTemplate.getForObject(url,Job.class);
        });
        try {
            Job job = supplier.get();
            return ResponseEntity.ok(job);
        }
        catch (HttpServerErrorException.InternalServerError e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Xin lỗi! Không thể kết nối đến service bạn mong muốn.");
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Xin lỗi! Không thể kết nối đến service bạn mong muốn 1.");
        }
    }

}

