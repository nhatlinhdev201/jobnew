package com.jobnew.analyzeservice.models;

public enum RecordStatus {
    PENDING("PENDING"),
    APPROVED("APPROVED"),
    REJECTED("REJECTED");

    private final String value;

    RecordStatus(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }

}
