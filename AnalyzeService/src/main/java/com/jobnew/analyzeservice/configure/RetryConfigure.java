package com.jobnew.analyzeservice.configure;

import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.retry.RetryRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.time.Duration;

@Configuration
public class RetryConfigure {
    @Bean
    public RetryRegistry retryRegistry() {
        return RetryRegistry.ofDefaults();
    }

    @Bean
    public Retry retry() {
        RetryConfig config = RetryConfig.custom()
                .maxAttempts(4)
                .waitDuration(Duration.ofMillis(3000))
                .build();
        return Retry.of("analyzeService", config);
    }
    @Bean
    public Retry analyzeServiceRetry(RetryRegistry retryRegistry) {
        return retryRegistry.retry("analyzeService");
    }
}
