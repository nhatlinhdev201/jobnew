package com.jobnew.loginservice.dto;

import com.jobnew.loginservice.models.Role;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthRequest {
    private String username;
    private String password;
    private Role role;
}
