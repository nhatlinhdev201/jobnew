package com.jobnew.loginservice.repository;

import com.jobnew.loginservice.models.UserCredential;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCredentialRepository extends MongoRepository<UserCredential, String> {
    UserCredential findByUsername(String username);
}
