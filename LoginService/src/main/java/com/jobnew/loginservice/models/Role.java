package com.jobnew.loginservice.models;

public enum Role {
    CANDIDATE("CANDIDATE"),
    HIRING("HIRING"),
    ADMIN("ADMIN");

    private final String value;
    Role(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
