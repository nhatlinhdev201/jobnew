package com.jobnew.loginservice.controller;

import com.jobnew.loginservice.dto.AuthRequest;
import com.jobnew.loginservice.dto.ResponseDto;
import com.jobnew.loginservice.models.UserCredential;
import com.jobnew.loginservice.service.AuthService;
import io.github.resilience4j.ratelimiter.RequestNotPermitted;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


@RestController
@EnableCaching
@RequestMapping("/auth")
public class AuthController {
    @Value("${user-service.url}")
    private String userServiceUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AuthService authService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @RateLimiter(name = "limitRegister")
    @PostMapping("/register")
    public ResponseEntity<UserCredential> addNewUser(@RequestBody UserCredential userCredential) {
        UserCredential user = new UserCredential();
        user.setUsername(userCredential.getUsername());
        user.setRole(userCredential.getRole());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<UserCredential> request = new HttpEntity<>(user, headers);
        ResponseEntity<UserCredential> response = restTemplate.postForEntity(userServiceUrl + "/users", request, UserCredential.class);
        if (response.getStatusCode().is2xxSuccessful()) {
            authService.saveUser(userCredential);
            return ResponseEntity.ok(userCredential);
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/login")
    public String getToken(@RequestBody AuthRequest authRequest) {
        Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        if (authenticate.isAuthenticated()) {
            return authService.generateToken(authRequest.getUsername());
        } else {
            throw new RuntimeException("Invalid username or password");
        }
    }
}
