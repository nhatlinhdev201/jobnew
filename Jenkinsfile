pipeline {
    agent any

    environment {
        DOCKER_CREDENTIALS_ID = 'dockerhub-credentials-id' // ID thông tin đăng nhập Docker Hub trong Jenkins
    }

    stages {
        stage('Checkout') {
            steps {
                // Kiểm tra mã nguồn từ kho lưu trữ
                checkout scm
            }
        }

        stage('Clean Old Docker Images') {
            steps {
                script {
                    // Xóa các Docker images cũ
                    def images = [
                        'nhatlinhdev201/discovery-service-image:v1.0',
                        'nhatlinhdev201/login-service-image:v1.0',
                        'nhatlinhdev201/api-gateway-service-image:v1.0',
                        'nhatlinhdev201/job-service-image:v1.0',
                        'nhatlinhdev201/notification-service-image:v1.0',
                        'nhatlinhdev201/record-service-image:v1.0',
                        'nhatlinhdev201/user-service-image:v1.0',
                        'nhatlinhdev201/analyze-service-image:v1.0'
                    ]
                    images.each { image ->
                        sh "docker rmi -f ${image} || true"
                    }
                }
            }
        }

        stage('Build and Push Docker Images') {
            parallel {
                stage('Build and Push Discovery Service') {
                    steps {
                        script {
                            buildAndPushDockerImage('nhatlinhdev201/discovery-service-image:v1.0')
                        }
                    }
                }
                stage('Build and Push Login Service') {
                    steps {
                        script {
                            buildAndPushDockerImage('nhatlinhdev201/login-service-image:v1.0')
                        }
                    }
                }
                stage('Build and Push API Gateway Service') {
                    steps {
                        script {
                            buildAndPushDockerImage('nhatlinhdev201/api-gateway-service-image:v1.0')
                        }
                    }
                }
                stage('Build and Push Job Service') {
                    steps {
                        script {
                            buildAndPushDockerImage('nhatlinhdev201/job-service-image:v1.0')
                        }
                    }
                }
                stage('Build and Push Notification Service') {
                    steps {
                        script {
                            buildAndPushDockerImage('nhatlinhdev201/notification-service-image:v1.0')
                        }
                    }
                }
                stage('Build and Push Record Service') {
                    steps {
                        script {
                            buildAndPushDockerImage('nhatlinhdev201/record-service-image:v1.0')
                        }
                    }
                }
                stage('Build and Push User Service') {
                    steps {
                        script {
                            buildAndPushDockerImage('nhatlinhdev201/user-service-image:v1.0')
                        }
                    }
                }
                stage('Build and Push Analyze Service') {
                    steps {
                        script {
                            buildAndPushDockerImage('nhatlinhdev201/analyze-service-image:v1.0')
                        }
                    }
                }
            }
        }

        stage('Deploy with Docker Compose') {
            steps {
                script {
                    // Khởi động lại các dịch vụ Docker Compose
                    sh 'docker-compose down'
                    sh 'docker-compose up -d'
                }
            }
        }

        stage('Health Check') {
            steps {
                script {
                    def services = [
                        'discovery-service': 8761,
                        'login-service': 8087,
                        'api-gateway-service': 8088,
                        'job-service': 8083,
                        'notification-service': 8084,
                        'record-service': 8086,
                        'user-service': 8082,
                        'analyze-service': 8085
                    ]
                    services.each { service, port ->
                        sh "docker-compose exec ${service} curl -f http://localhost:${port}/actuator/health || exit 1"
                    }
                }
            }
        }
    }

    post {
        always {
            // Luôn luôn dọn dẹp sau khi build
            script {
                sh 'docker-compose down'
            }
        }
    }
}

def buildAndPushDockerImage(String imageName) {
    docker.withRegistry('https://index.docker.io/v1/', env.DOCKER_CREDENTIALS_ID) {
        docker.build(imageName).push()
    }
}
