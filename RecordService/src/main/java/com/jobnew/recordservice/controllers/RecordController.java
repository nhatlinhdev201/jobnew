package com.jobnew.recordservice.controllers;

import com.jobnew.recordservice.models.Record;
import com.jobnew.recordservice.models.RecordStatus;
import com.jobnew.recordservice.services.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/api/record/v1")
public class RecordController {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RecordService recordService;

    @GetMapping("/records/{id}")
    public ResponseEntity<Record> getReccordById(@PathVariable("id") String id) {
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @PostMapping("/records")
    public ResponseEntity<Record> createRecord(@RequestBody Record record) {
        Record createdRecord = recordService.createRecord(record);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdRecord);
    }
    @GetMapping("/status")
    public List<Record> findRecordsByStatus(@RequestParam RecordStatus status) {
        return recordService.findRecordByStatus(status);
    }

    @GetMapping("/records")
    public List<Record> getAllRecords() {
        return recordService.getAlRecord();
    }
}
