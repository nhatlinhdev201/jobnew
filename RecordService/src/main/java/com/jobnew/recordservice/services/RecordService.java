package com.jobnew.recordservice.services;

import com.jobnew.recordservice.models.Record;
import com.jobnew.recordservice.models.RecordStatus;
import com.jobnew.recordservice.repositories.RecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class RecordService {
    @Autowired
    private RecordRepository recordRepository;

    public Record createRecord(Record record) {
        return recordRepository.save(record);
    }

    public List<Record> getAlRecord() {
        return recordRepository.findAll();
    }

    public Optional<Record> getRecordById(String id) {
        return recordRepository.findById(id);
    }

    public Optional<Record> changeStatusRecord(String id, RecordStatus status) {
        if(recordRepository.existsById(id)) {
            Record updatedRecord = recordRepository.findById(id).get();
            updatedRecord.setStatus(status);
            return Optional.of(recordRepository.save(updatedRecord));
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"id user is not exits !");
        }
    }

    public List<Record> findRecordByStatus(RecordStatus status) {
        return recordRepository.findAllByStatus(status);
    }
}
