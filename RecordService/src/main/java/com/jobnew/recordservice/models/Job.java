package com.jobnew.recordservice.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "job")
public class Job {
    @Id
    @Field("id")
    private String id;
    @Field("createrId")
    private String createrId;
    @Field("type")
    private String type;
    @Field("detailJob")
    private String detailJob;
    @Field("salary")
    private String salary;
    @Field("status")
    private boolean status;
    @Field("createAt")
    private Date createAt;
    @Field("updateAt")
    private Date updateAt;

}
