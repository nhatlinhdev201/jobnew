package com.jobnew.recordservice.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Document(collection = "record")
public class Record {
    @Id
    private String id;

    @Field("jobId")
    private String jobId;

    @Field("candidateId")
    private String candidateId;

    @Field("status")
    private RecordStatus status;

    @Field("createAt")
    private Date createAt;
}
