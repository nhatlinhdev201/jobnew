package com.jobnew.recordservice.repositories;

import com.jobnew.recordservice.models.Record;
import com.jobnew.recordservice.models.RecordStatus;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecordRepository extends MongoRepository<Record, String> {
    public List<Record> findAllByStatus(RecordStatus status);
}
