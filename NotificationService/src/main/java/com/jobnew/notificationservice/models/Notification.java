package com.jobnew.notificationservice.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "notification")
public class Notification {
    @Id
    @Field("id")
    private String id;
    @Field("jobId")
    private String jobId;
    @Field("receiverEmail")
    private String receiverEmail;
    @Field("detail")
    private String detail;
    @Field("createAt")
    private Date createAt;
}

