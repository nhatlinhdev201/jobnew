package com.jobnew.notificationservice.controller;
import com.jobnew.notificationservice.models.Notification;
import com.sendgrid.*;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Date;

@Controller
@RestController
@RequestMapping("/api/notification/v1")
public class SendMailController {
    @PostMapping("/sendEmailTrungTuyen")
    public String sendEmailTrungTuyen(@RequestBody NotificationReq notificationReq) {
//        System.out.println(email.getEmail());
        String apiKey = "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";
        Email from = new Email("baotruc123boo@gmail.com");
        String subject = "Thông báo chúc mừng trúng tuyển";
        Email to = new Email(notificationReq.getEmail());
        Content content = new Content("text/plain", "Kính gửi "+notificationReq.getNameUCV()+"," +
                "Chúng tôi xin gửi lời chúc mừng chân thành đến bạn vì việc trúng tuyển vào vị trí "+notificationReq.getPosition()+" tại "+notificationReq.getNameNTD()+"." +
                "Đây là một bước quan trọng trong sự nghiệp của bạn và chúng tôi tin rằng bạn sẽ đậu phỏng vấn. " +
                "Hãy vào trang web của chúng tôi để liên hệ nhận lịch phỏng vấn với công ty.");
        Mail mail = new Mail(from, subject, to, content);
        SendGrid sg = new SendGrid(apiKey);
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            if(response.getStatusCode()==202)
            {
                return "Đã gửi email thành công";
            }else
            {
                return "Gửi email thất bại";
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return "Đã gửi email thành công";
    }
}
class NotificationReq {
    private String email;
    private String nameUCV;
    private String nameNTD;
    private String position;

    // Getter và setter cho các trường
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNameUCV() {
        return nameUCV;
    }

    public void setNameUCV(String nameUCV) {
        this.nameUCV = nameUCV;
    }

    public String getNameNTD() {
        return nameNTD;
    }

    public void setNameNTD(String nameNTD) {
        this.nameNTD = nameNTD;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}