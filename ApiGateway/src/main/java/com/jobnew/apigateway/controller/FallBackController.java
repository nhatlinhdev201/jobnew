package com.jobnew.apigateway.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fallback")
public class FallBackController {
    @GetMapping("/loginServiceFallback")
    public ResponseEntity<String> fallbackLoginService() {
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("[CircuitBreaker - ApiGateway] Service Login is not available");
    }

    @GetMapping("/jobServiceFallback")
    public ResponseEntity<String> fallbackJobService() {
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("[CircuitBreaker - ApiGateway] Service Jobs is not available");
    }

    @GetMapping("/recordServiceFallback")
    public ResponseEntity<String> fallbackRecordService() {
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("[CircuitBreaker - ApiGateway] Service Record is not available");
    }

    @GetMapping("/analyzeServiceFallback")
    public ResponseEntity<String> fallbackAnalyzeService() {
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("[CircuitBreaker - ApiGateway] Service Analyze is not available");
    }

    @GetMapping("/notifyServiceFallback")
    public ResponseEntity<String> fallbackNotifyService() {
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("[CircuitBreaker - ApiGateway] Service Supplier is not available");
    }

    @GetMapping("/userServiceFallback")
    public ResponseEntity<String> fallbackUserService() {
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("[CircuitBreaker - ApiGateway] Service User is not available");
    }
}
